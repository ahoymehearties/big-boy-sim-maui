﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    internal class Friend
    {
        public string Name { get; set; }
        public Friend(string friendname)
        {
           this.Name = friendname;
        }

        public virtual void Play()
        {

        }
    }
}
