﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    internal class Controls
    {
        public static bool playing = true;
        public static Room currentRoom;
        public static Random random = new Random();

        static void Look()
        {
            Console.WriteLine(currentRoom.Name);
            Console.WriteLine(currentRoom.Description);
        }

        public static string GetCurrentRoom()
        {
            return currentRoom.Name;
        }

        public static string DisplayEvents()
        {
            return $"You are in the {currentRoom.Name}.\n" +
                $"{currentRoom.Description}";
        }

        
        public static void Go(Actions action)
        {

            switch (action)
            {
                case Actions.North:
                    Console.WriteLine("Go North");
                    break;
                case Actions.South:
                    Console.WriteLine("Go South");
                    break;
                case Actions.East:
                    Console.WriteLine("Go East");
                    break;
                case Actions.West:
                    Console.WriteLine("Go West");
                    break;
                case Actions.NorthWest:
                    Console.WriteLine("Go NorthWest");
                    break;
                case Actions.NorthEast:
                    Console.WriteLine("Go NorthEast");
                    break;
                case Actions.SouthWest:
                    Console.WriteLine("Go SouthWest");
                    break;
                case Actions.SouthEast:
                    Console.WriteLine("Go SouthEast");
                    break;
                case Actions.Look:
                    Look();
                    BigBoy.CheckFood();
                    BigBoy.CheckHunger();
                    break;
                case Actions.Meow:
                     BigBoy.Meow();
                    BigBoy.CheckFood();
                    BigBoy.CheckHunger();
                    break;
                case Actions.PoopPoop:
                     BigBoy.PoopPoop();
                    BigBoy.CheckFood();
                    BigBoy.CheckHunger();
                    break;
                case Actions.Groom:
                    BigBoy.LickSelf();
                    BigBoy.CheckFood();
                    BigBoy.CheckHunger();
                    break;
                case Actions.Cover:
                    BigBoy.CoverPoop();
                    BigBoy.CheckFood();
                    BigBoy.CheckHunger();
                    break;
                case Actions.Scratch:
                    BigBoy.Scratch();
                    BigBoy.CheckFood();
                    BigBoy.CheckHunger();
                    break;
                case Actions.Sleep:
                    BigBoy.Sleep();
                    BigBoy.CheckFood();
                    BigBoy.CheckHunger();
                    break;
                case Actions.Eat:
                    BigBoy.CheckFood();
                    BigBoy.Eat();
                    BigBoy.CheckHunger();
                    break;
            }

            if (currentRoom.Exits.ContainsKey(action))
            {
                Console.Clear();
                currentRoom = currentRoom.Exits[action];
                Look();
                BigBoy.CheckFood();
                int dadaInvasionChance = random.Next(0, 100);
                if (dadaInvasionChance <= 20)
                {
                    Events.DadaInvasion();
                }
                Console.WriteLine($"\nYour current comfort level is {BigBoy.GetComfortLevel()}");
            }

            /*
            else
            {
                Console.WriteLine("Despite your best efforts you are unable to move to the " + action);
            }
            */
        }

        public static Actions ConvertStringToDirection(string decision)
        {
            decision = decision.ToLower();
            switch (decision)
            {
                case "n":
                case "north":
                    return Actions.North;
                case "s":
                case "south":
                    return Actions.South;
                case "e":
                case "east":
                    return Actions.East;
                case "w":
                case "west":
                    return Actions.West;
                case "ne":
                case "northeast":
                    return Actions.NorthEast;
                case "nw":
                case "northwest":
                    return Actions.NorthWest;
                case "sw":
                case "southwest":
                    return Actions.SouthWest;
                case "se":
                case "southeast":
                    return Actions.SouthEast;
                case "look":
                    return Actions.Look;
                case "poop poop":
                    return Actions.PoopPoop;
                case "meow":
                    return Actions.Meow;
                case "eat":
                    return Actions.Eat;
                case "groom":
                    return Actions.Groom;
                case "cover poop":
                    return Actions.Cover;
                case "scratch":
                    return Actions.Scratch;
                case "sleep":
                    return Actions.Sleep;


            }
            return default(Actions);
        }
    }
}
