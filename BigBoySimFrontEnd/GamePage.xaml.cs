namespace BigBoySimFrontEnd;
using BigBoySimulator;

public partial class GamePage : ContentPage
{
	public GamePage()
	{
		InitializeComponent();
        Controls.currentRoom = Room.ListOfRooms["office"];
        GameEvents.Text = Controls.DisplayEvents();
	}
}