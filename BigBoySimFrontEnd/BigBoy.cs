﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    enum MeowTypes
    {
        Rrrow,
        Mrrrow,
        Meow,
        Mow,
        Rar,
        Mmkay
    }
    static class BigBoy
    {
        static Random random = new Random();
        static int comfortLevel = 20;
        static double weight = 12.3;
        static bool _notHungry = true;
        static bool _hasEaten = false;
        public static string stealMe = "You are stealing this string from the BigBoy class!";

        public static bool HasEaten { get { return _hasEaten; } }

        public static bool TummyFull {
            get { return _notHungry; }
            set { } 
        }

        private static bool hasCrusties = false;
        public static bool HasCrusties
        {
            get { return hasCrusties; }
            private set { }
        }

        static string name = "Big Boy";
        public static string Name
        {
            get { return name; }
            private set { name = value; }
        }

        public static void ShowComfortandTime()
        {
            Console.WriteLine($"Your comfort level is {comfortLevel} and the time is {TimeController.Time}. {BellyFull()}");
        }

        public static string GetBigBoyStatus()
        {
            return $"Your comfort level is {comfortLevel} and the time is {TimeController.Time}.";
        }

        // This function is garbage and makes no sense
        public static string BellyFull()
        {
            if (_notHungry)
            {
                return $"You're only moderately hungry, but that's usual for you. {_notHungry}";
            }
            else
            {
                return "You're so fuckin' hungry! You think you are starving to death!!";
            }

        }

        public static void Meow()
        {
            int number = random.Next(2, 7);
            for (int i = 0; i < number; i++)
            {
                var meow = (MeowTypes)random.Next(0, 5);
                TimeController.AddMinutes(1);
                Console.WriteLine($"{meow}!");
            }
        }

        public static void Hiss()
        {
            Console.WriteLine("You hiss! Dada laughs and calls you a moron. You *hate* when Dada laughs.");
            ChangeComfortLevel(-5);
            TimeController.AddMinutes(1);
            ShowComfortandTime();

        }

        public static void PoopPoop()
        {
            Meow();
            if (Controls.GetCurrentRoom() != "The Litterbox")
            {
                Console.WriteLine("Mama screams, and Dada laughs his terrifying laugh!");
                ChangeComfortLevel(-10);
                ShowComfortandTime();
            }
            else if (Controls.GetCurrentRoom() == "The Litterbox")
            {
                LitterBox.AddPoops(new Poop());
                ChangeComfortLevel(2);
                ShowComfortandTime();
            }
            hasCrusties = true;
            
        }

        public static void NameChange(string newname)
        {
            name = newname;
        }

        public static int GetComfortLevel()
        {
            return comfortLevel;
        }

        public static void ClearCrusties()
        {
            hasCrusties = false;
        }

        public static void ChangeComfortLevel(int number)
        {
            comfortLevel += number;
            if (comfortLevel < 0)
            {
                Die();
            }
        }

        public static void Eat()
        {
            if (TimeController.IsFoodTime())
            {
                if (Controls.GetCurrentRoom() == "Office")
                {
                    Console.WriteLine("You eat da food!");
                    ChangeComfortLevel(20);
                    weight += 0.5;
                    _notHungry = true;
                    _hasEaten = true;
                }
                else if (Controls.GetCurrentRoom() != "Office")
                {
                    Console.WriteLine("The food isn't here! RUN! RUN before it is gone for good!!");
                }
            }
            else
            {
                Console.WriteLine("It's not time to eat!");
            }
        }

        // This function is better than BellyFull() and needs to be cleaned up and Check text added
        public static bool CheckHunger()
        {
            if (TimeController.HungerTime() && !_hasEaten)
            {
                _notHungry = false;
                _hasEaten = false;
                return true;
            }
            return false;
        }
        public static void CheckFood()
        {
            if (TimeController.IsFoodTime())
            {
                Console.WriteLine("It's food time!!! Go to the office!");
            }
        }

        public static void Scratch()
        {
            Console.WriteLine("Would you like to scratch the wall or a scratchpad?");
            var decision = Console.ReadLine();

            if (decision == "wall" || decision == "w")
            {
                int number = random.Next(1, 100);
                Console.WriteLine("You scratch the wall. It is both hard and soft and yields easily to your claws. It feels really good.");
                ChangeComfortLevel(10);
                if (number >= 30)
                {
                    Console.WriteLine("Mama yells and runs toward you. She intends to kill you!");
                    ChangeComfortLevel(-20);
                }
                if (number < 30)
                {
                    Console.WriteLine("If mama saw you, she would have killed you, but she didn't notice. You're in the clear!");
                }
                TimeController.AddMinutes(3);
                ShowComfortandTime();


            }
            else if (decision == "scratchpad" || decision == "s")
            {
                int number = random.Next(0, 1);
                Scratchpad scratchpad = Controls.currentRoom.scratchpads[number];

                scratchpad.Scratch();
                Console.WriteLine("You scratch a scratchpad. It feels pretty good!");
                if (scratchpad.Scratchability >= 80)
                {
                    ChangeComfortLevel(5);
                    Console.WriteLine($"Because the scratchpad is in great condition {scratchpad.Scratchability}%, you gain 5 comfort points!");
                }
                TimeController.AddMinutes(3);
                ShowComfortandTime();

            }

        }
        public static void CoverPoop()
        {
            Console.WriteLine("You are so tired and the iPad has some good birds on it you'd rather watch, but you summon the will to cover your fecal matter.");
            LitterBox.CoverAllPoops();
            TimeController.AddMinutes(5);
            LitterBox.PoopTally();
            ShowComfortandTime();
        }

        public static void Die()
        {
            Console.WriteLine("The horror of the world is too much! You get a heart attack and die.");
            Controls.playing = false;
            
        }

        public static void LickSelf()
        {
            
            if (hasCrusties)
            {
                Console.WriteLine("You have crusties. They don't look very tasty. Would you like to clean them?");
                var decision = Console.ReadLine();

                if (decision == "y" || decision == "yes")
                {
                    ClearCrusties();
                    ChangeComfortLevel(-1);
                    Console.WriteLine("You clean your crusties, and you wish you hadn't. They taste like shit. At least you feel cleaner.");
                }
            }
            ChangeComfortLevel(5);
            Console.WriteLine("You lick yourself all over, and even nibble at your empty scrotum.");
            TimeController.AddMinutes(15);
            ShowComfortandTime();

        }

        public static void Sleep()
        {
            int hours;
            Console.WriteLine("How many hours would you like to sleep?");
            bool validHours = int.TryParse(Console.ReadLine(), out hours);
            if (validHours)
            {
                TimeController.AddHours(hours);
                ChangeComfortLevel(hours * 5);
                ShowComfortandTime();
            }
            else
            {
                Console.WriteLine("Not a number. Try again.");
            }
            
            
        }
    }
}
