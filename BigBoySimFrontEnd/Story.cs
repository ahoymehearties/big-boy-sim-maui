﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimFrontEnd
{
    static class Story
    {
        private static string[] storylines = new string[20] {
            "You are a cat. All you knew were the mean streets of a temperate island.",
            "You were kicked and laughed at by bearded humans with funny hats. They yelled at you in gibberish human language.",
            "You learned to skulk in shadows, hiding in dumpsters and small spaces.",
            "You came out for one thing, and one thing alone: food.",
            "Food! Glorious food! How wonderful, how divine it is to you that it exists! You will do anything to get it.",
            "You learned to be quick, stealing morsels wherever you could with vicious hunger. In fact, you grew to be quite fat, and other cats knew to stay out of your way.",
            "(Wide Load)",
            "At least, until the crazy human female with the furry arms arrived.",
            "She snatched your kind up like candy, and where she sent them, you had no idea, but you never saw them again.",
            "One day, she came for you. She was tricksy. False. She offered you food, and life seemed good.",
            "You didn't have to hunt. You ate from morning until night, and sometimes through the night. You were happy.",
            "But it was all a massive deception, because soon, you found yourself shoved into a cage, rattled about for hours, and thrown into a cold, vast expanse where you heard nothing but the wails of your fellow damned.",
            "Your ears hurt from the pressure, and you were certain that you were being taken to be turned into food for some other creature.",
            "Many humans handled your cage after that, jostling you and shaking you around. You rode on strange moving black carpet, until you were snatched up by the crazy furry-armed human.",
            "You were brought into a quiet room, and the furry-armed human grabbed you by the scruff of your neck to shove you into a different prison.",
            "You hissed at her, but she didn't see things quite the way you did.",
            "After that, a strange human female took your cage very gently. You were terrified, and hid in the of your torture chamber.",
            "Thankfully, she covered the cage with a blanket so you wouldn't have to see the confusing sights around you.",
            "You prayed for death.",
            "Then, finally, quiet. Your cage was opened, and you witnessed a strange new world."
        };
        
        public static string GetStory(int line)
        {
            return storylines[line - 1];
        }


    }
}
