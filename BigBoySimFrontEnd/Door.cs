﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    internal class Door
    {
        private bool isOpen = false;
        public bool IsOpen
        {
            get { return isOpen; }
            private set { }
        }
        public string Name { get; set; }

        public Door(string doorName, bool open)
        {
            this.isOpen = open;
            this.Name = doorName;
        }

        private List<Door> doors = new List<Door>() { new Door("kitchen", true), new Door("dada", false), new Door("office", true), new Door("foyer", true), new Door("bedroom", false), new Door("livingroom", false), new Door("bathroom", true), new Door("outside", false)};

        public List<Door> GetDoors()
        {
            return doors;
        }
        
    }
}
