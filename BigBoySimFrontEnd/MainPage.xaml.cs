﻿namespace BigBoySimFrontEnd;
using BigBoySimulator;
using CommunityToolkit.Maui;

public partial class MainPage : ContentPage
{
	int count = 0;
	int storyPages = 0;

	public MainPage()
	{
		InitializeComponent();
	}

	private void OnCounterClicked(object sender, EventArgs e)
	{
		count++;

		if (count == 1)
			CounterBtn.Text = $"Clicked {count} time";
		else
			CounterBtn.Text = $"Clicked {count} times";

		SemanticScreenReader.Announce(CounterBtn.Text);
	}

	private void MainContinueClicked(object sender, EventArgs e)
	{
		storyPages++;
		StoryBlock.Text = Story.GetStory(storyPages);

		// If at the last index of the array, load the next page


    }

    private void OnStartClicked(object sender, EventArgs e)
    {
        // Open the BigBoyStory.xaml page, below is broken
        // await Navigation.PushAsync(new BigBoyStory());

    }
}

