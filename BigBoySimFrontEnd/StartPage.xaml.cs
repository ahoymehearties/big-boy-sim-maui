namespace BigBoySimFrontEnd;


public partial class StartPage : ContentPage
{
    int count = 0;
    int storyPages = 0;
    public StartPage()
	{
		InitializeComponent();
	}
    private void MainContinueClicked(object sender, EventArgs e)
    {
        storyPages++;
        StoryBlock.Text = Story.GetStory(storyPages);

        // If at the last index of the array, load the next page


    }

    private async void OnStartClicked(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("GamePage");

    }
}